#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include "Types.h"
#include "Characteristic.h"
#include "drawer.h"
#include <sstream>
#include "dop.h"
#include <fstream>



TEST_CASE("PrintNames")
{
	Circle c1(1), c2(2);
	Triangle t(0,0,1,2,3,4);
	Named n("new_name");
	class Rectangle r(10,10);
	std::string s;
	s = printNames(c1,c2,t,n,r);
	REQUIRE(s == "Circle.1 Circle.2 Triangle.1 new_name.1 Rectangle.1");
}

TEST_CASE("Constructor & Square")
{
	Circle c(10);
	class Rectangle r(2,5);
	REQUIRE(3.14159 * pow(10,2) == c.square());
	REQUIRE(10 == r.square());
	Triangle t(0,0,1,1,3,4);
	double S = t.square();
	REQUIRE((0.5 - S) < 0.1);
}

TEST_CASE("Color")
{
	Circle c (10);

	c.setColor("navy");
	std::tuple <int,int,int> col;
	std::tuple <int,int,int> col1 (0,0,128);
	col = c.getColored();
	REQUIRE(col == col1);

	class Rectangle r (2,5);

	r.setColor("green");
	col = c.getColored();

	std::tuple <int,int,int> col2 (0,128,0);
	REQUIRE(col == col1);
}

TEST_CASE("totalArea")
{
	class Rectangle r(10,10);
	Circle c(1);
	Triangle t(0,0,0,10,10,0);
	REQUIRE((totalArea(t,c,r) - 153.14) < 0.1);
}

TEST_CASE("Drawer")
{
	stringstream s;
	string out = "example.bmp";
	s << "800x800" << endl;
	s << "Rectangle(100, 200) [600, 600] {green}"<< endl;
	s << "Circle(100) [150, 150] {127, 127, 255}"<< endl;
	s << "Triangle(400, 200, 100, 200) [250, 300] {255, 0, 0}"<<endl;
	drawer(s,out);
}

TEST_CASE("Drawer_file")
{
	ifstream s;
	s.open("primer.txt");
	string out = "example.bmp";
	drawer(s,out);
	s.close();
}

//
// Created by 79137 on 02.12.2019.
//

#ifndef CPP_TASK3_DRAWER_TYPES_H
#define CPP_TASK3_DRAWER_TYPES_H

#include "Shape.h"
#include <memory>
#include <cmath>

class Circle: public Shape
{
private:
    int r;
public:
    explicit Circle(int);
    explicit Circle(std::vector<int>);
    void setCenter (int, int) override;
    double square() const  override;
    std::vector <Segments> getSegments() const override;
};


class Rectangle: public Shape
{
private:
    int w;
    int h;
public:
    explicit Rectangle(int, int);
    explicit Rectangle(vector<int>);
    void setCenter (int, int) override;
    double square() const  override;
    vector <Segments> getSegments() const override;
};


class Triangle: public Shape
{
private:
    pair <int, int> x;
    pair <int,int> y;
public:
    explicit Triangle(vector <int>);
    ~Triangle() = default;
    explicit Triangle(int, int, int, int, int, int);
    void setCenter (int, int) override;
    double square() const  override;
    vector <Segments> getSegments() const override;
};
#endif //CPP_TASK3_DRAWER_TYPES_H

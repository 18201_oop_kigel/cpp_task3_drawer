
#include "Shape.h"


pair<int,int> Segments::getStart() const {return start;};

pair<int,int> Segments::getEnd() const {return end;};

void Shape::setCenter(int x, int y)
{
	center = std::make_pair(x,y);
}

string Shape::getName() const
{
	return name;
}
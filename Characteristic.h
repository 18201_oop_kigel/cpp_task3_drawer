//
// Created by 79137 on 02.12.2019.
//


#ifndef CPP_TASK3_DRAWER_CHARACTERISTIC_H
#define CPP_TASK3_DRAWER_CHARACTERISTIC_H


#include <map>
#include <string>
#include <tuple>
#include <sstream>

class Named
{
private:
   static std::map <std::string, unsigned int> count;

public:

    Named(const std::string &);
    std::string name;
    virtual ~Named() = default;
    explicit Named() = default;
};

class Colored
{
private:
    int r;
    int g;
    int b;
public:
    virtual ~Colored() = default;
    Colored() = default;
    void setColor(unsigned int,unsigned int,unsigned int);
    void setColor(const std::string &);
    std::tuple<int,int,int> getColored();
};

#endif //CPP_TASK3_DRAWER_CHARACTERISTIC_H

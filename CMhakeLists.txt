cmake_minimum_required(VERSION 3.13)
project(cpp_task3_drawer)

set(CMAKE_CXX_STANDARD 14)

include_directories(cpp_task3_drawer)

add_executable(cpp_task3_drawer
        cpp_task3_drawer/build/.gitkeep
        cpp_task3_drawer/.gitignore
        cpp_task3_drawer/.gitlab-ci.yml
        cpp_task3_drawer/catch.hpp
        cpp_task3_drawer/CMakeLists.txt
        cpp_task3_drawer/Shape.cpp
        cpp_task3_drawer/Shape.h
        cpp_task3_drawer/main.cpp
        cpp_task3_drawer/test.cpp)

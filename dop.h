//
// Created by 79137 on 17.12.2019.
//

#ifndef CPP_TASK3_DRAWER_DOP_H
#define CPP_TASK3_DRAWER_DOP_H

#include "Types.h"

template<typename... Arg>
double totalArea(Shape const & F, Arg... args)
{
    return F.square() + totalArea(args...);
}
inline double totalArea(Shape const & F)
{
    double res = F.square();
    return res;
}


template <typename... arg>
string printNames(Named const & Y_name, arg... args)
{
    return Y_name.name + " " + printNames(args...);
}

inline string printNames(Named const & Y_name)
{
    string name = Y_name.name;
    return name;
}

std::vector<unsigned int> StColor_in_Int(const string &);

#endif //CPP_TASK3_DRAWER_DOP_H

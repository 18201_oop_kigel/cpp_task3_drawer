//
// Created by 79137 on 11.12.2019.
//
#include "drawer.h"
#include <cstdlib>

using namespace std;

void drawer(istream &in, string & out)
{
    string xPolya;

    getline(in,xPolya);

    unsigned int pos = xPolya.find('x');

    string yPolya = xPolya.substr(pos + 1);

    xPolya = xPolya.substr(0, pos);

    bitmap_image img(atoi(xPolya.c_str()), atoi(yPolya.c_str()));
    img.set_all_channels(128, 128, 128);
    image_drawer draw(img);
    draw.pen_width(3);

    std::string new_str;

    vector <int> charac;
    vector <int> center;
    vector <unsigned int> color;
    string s_color;

    int j = 0;

    while(getline(in,new_str))
    {
        string name;
        int characteristic = 0;

        unsigned int pos_n = new_str.find('(');

        name = new_str.substr(0, pos_n);

        unsigned int i = pos_n + 1;

        do
        {
            if(new_str[i] == ',' || new_str[i] == ')')
            {
                charac.push_back(characteristic);
                characteristic = 0;
            }
            else if(new_str[i] == ' ')
            {
                ++i;
                continue;
            }
            else
            {
                characteristic = characteristic * 10 + (new_str[i] - '0');
            }
            ++i;
        }while(new_str[i - 1] != ')');

        i += 1;

        do
        {
            ++i;
            if(new_str[i] == ',' || new_str[i] == ']')
            {
                center.push_back(characteristic);
                characteristic  = 0;
            }
            else if(new_str[i] == ' ')
            {
                continue;
            }
            else
            {
                characteristic = characteristic * 10 + (new_str[i] - '0');
            }
        }
        while(new_str[i - 1] !=  ']');

        i+= 2;
        if (!isdigit(new_str[i]))
        {
            do
                {
                s_color += new_str[i];
                i++;
            }
            while (new_str[i] != '}');
        }
        else
        {
            i--;
            do
            {
                i++;
                if (new_str[i] == ',' || new_str[i] == '}')
                {
                    color.push_back(characteristic);
                    characteristic = 0;
                }
                else if (new_str[i] == ' ')
                {
                    continue;
                }
                else
                    characteristic = characteristic * 10 + (new_str[i] - '0');
            } while (new_str[i - 1] != '}');
        }

        if(!s_color.empty())
        {
            color = StColor_in_Int(s_color);
            draw.pen_color(color[0],color[1],color[2]);
        }
        else if(!color.empty())
        {
           draw.pen_color(color[0], color[1],color[2]);
        }
        else
        {
            draw.pen_color(0,0,0);
        }


        if(name == "Triangle")
        {
            Triangle t (charac);
            t.setCenter(center[0],center[1]);
            if(!s_color.empty())
            {
                t.setColor(s_color);
            }
            else
            {
                t.setColor(color[0],color[1],color[2]);
            }

            std::vector<Segments> otrezki = t.getSegments();


            for(auto &k : otrezki)
            {
                draw.line_segment(k.getStart().first,k.getStart().second,
                                  k.getEnd().first,k.getEnd().second);
            }

            s_color.erase();
            color.clear();
            charac.clear();
            center.clear();

        }

        if(name ==  "Rectangle")
        {
            Rectangle t (charac) ;
            t.setCenter(center[0],center[1]);
            if(!s_color.empty())
            {
                t.setColor(s_color);
            }
            else
            {
                t.setColor(color[0],color[1],color[2]);
            }
            std::vector<Segments> otrezki = t.getSegments();

            for(auto &k : otrezki)
            {
                draw.line_segment(k.getStart().first,k.getStart().second,
                                  k.getEnd().first,k.getEnd().second);
            }

            s_color.erase();
            color.clear();
            charac.clear();
            center.clear();
        }

        if(name == "Circle")
        {
            Circle t (charac) ;
            t.setCenter(center[0],center[1]);
            if(!s_color.empty())
            {
                t.setColor(s_color);
            }
            else
            {
                t.setColor(color[0],color[1],color[2]);
            }
            std::vector<Segments> otrezki = t.getSegments();

            for(auto &k : otrezki)
            {
                draw.line_segment(k.getStart().first,k.getStart().second,
                                  k.getEnd().first,k.getEnd().second);
            }
            s_color.erase();
            color.clear();
            charac.clear();
            center.clear();
        }
        j++;
    }
    
    img.save_image(out);
}
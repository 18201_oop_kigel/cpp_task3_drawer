//
// Created by 79137 on 03.12.2019.
//


#define  pi 3.14159

#include "Types.h"


Circle::Circle(int radius)
{
    r = radius;
    name = Named("Circle").name;
    setColor(0,0,0);
}

Circle::Circle(std::vector<int> radius)
{
    r = radius[0];
    name = Named("Circle").name;
    setColor(0,0,0);
}

std::vector<Segments> Circle::getSegments() const
{
    std::vector <Segments> exit;
    double x = center.first;
    double y = center.second;

    double x1 = x;
    double y1 = y + r;
    double x2 = x + r * sin(pi/18);//10 градусов поворот
    double y2 = y + r * cos(pi/18);

    for(int i = 2; i < 11; i++)
    {
        exit.emplace_back(x1,y1,x2,y2);
        x1 = x2;
        y1 = y2;
        x2 = x + r * sin(pi*i/18);
        y2 = y + r * cos(pi*i/18);
    }

    x1 = x + r;
    y1 = y;
    x2 = x + r * cos(pi/18);
    y2 = y - r * sin(pi/18);


    for(int i = 2; i < 11; i++)
    {
        exit.emplace_back(x1,y1,x2,y2);
        x1 = x2;
        y1 = y2;
        x2 = x + r * cos(pi*i/18);
        y2 = y - r * sin(pi*i/18);
    }

    x1 = x;
    y1 = y - r;
    x2 = x  -  r * sin(pi/18);
    y2 = y  - r * cos(pi/18);


    for(int i = 2; i < 11; i++)
    {
        exit.emplace_back(x1,y1,x2,y2);
        x1 = x2;
        y1 = y2;
        x2 = x - r * sin(pi*i/18);
        y2 = y -  r* cos(pi*i/18);
    }


    x1 = x - r;
    y1 = y;
    x2 = x  -  r* cos(pi/18);
    y2 = y  + r * sin(pi/18);


    for(int i = 2; i < 11; i++)
    {
        exit.emplace_back(x1,y1,x2,y2);
        x1 = x2;
        y1 = y2;
        x2 = x - r * cos(pi*i/18);
        y2 = y  + r* sin(pi*i/18);
    }

    return exit;
}
double Circle::square() const
{
    return pi * pow(r,2);
}

void Circle::setCenter(int x, int y)
{
    center = std::make_pair(x,y);
}

Rectangle::Rectangle(int width, int height)
{
    w = width;
    h = height;
    center = std::make_pair(0,0);
    name = Named ("Rectangle").name;
    setColor(0,0,0);
}

Rectangle::Rectangle(std::vector<int> z)
{
    w = z[0];
    h = z[1];
    center = std::make_pair(0,0);
    name = Named("Rectangle").name;
    setColor(0,0,0);
}

double Rectangle::square() const
{
    return h*w;
}

void Rectangle::setCenter(int x, int y)
{
    center = std::make_pair(x,y);
}

std::vector<Segments> Rectangle::getSegments() const
{
    std::vector<Segments> exit;
    int x = center.first;
    int y = center.second;
    exit.emplace_back(x + w/2,y + h/2, x + w/2,y - h/2);
    exit.emplace_back(x + w/2,y - h/2, x - w/2,y - h/2);
    exit.emplace_back(x - w/2,y - h/2, x - w/2,y + h/2);
    exit.emplace_back(x - w/2,y + h/2, x + w/2,y + h/2);

    return exit;

}

Triangle::Triangle(int x1, int y1, int x2, int y2, int x3, int y3)
{
    center = std::make_pair(x1, y1);
    x = std::make_pair(x3 + x1, y3 + y1);
    y = std::make_pair(x2 + x1, y2 + y1);
    name = Named("Triangle").name;
    setColor(0,0,0);
}

Triangle::Triangle(std::vector<int> z)
{
    center = std::make_pair(0,0);

    x = std::make_pair(center.first + z[0], center.second + z[1]);
    y = std::make_pair(center.first + z[2], center.second + z[3]);

    name = Named("Triangle").name;
    setColor(0,0,0);
}

void Triangle::setCenter(int a, int b)
{
    int x1 =  - center.first + x.first;
    int y1 =  - center.second + x.second;
    int x2 = - center.first + y.first;
    int y2 = - center.second + y.second;
    center = std::make_pair(a,b);
    x = std::make_pair(x1 + center.first,y1 + center.second);
    y = std::make_pair(x2 + center.first,y2 + center.second);
}

double Triangle::square() const
{
    double c = sqrt(pow(x.first - y.first, 2) + pow(x.second - y.second,2));
    double b = sqrt(pow(center.first - x.first, 2) + pow(center.second - x.second,2));
    double a = sqrt(pow(center.first - y.first, 2) + pow(center.second - y.second,2));
    double p = (a + b + c)/2.0;
    return sqrt(p*(p-a)*(p -b)*(p -c));
}

std::vector<Segments> Triangle::getSegments() const
{
    std:: vector<Segments> exit;

    exit.emplace_back(std::make_pair(center.first, center.second),std::make_pair(x.first, x.second));
    exit.emplace_back(std::make_pair(x.first, x.second),std::make_pair(y.first, y.second));
    exit.emplace_back(std::make_pair(y.first, y.second),std::make_pair(center.first, center.second));

    return exit;
}
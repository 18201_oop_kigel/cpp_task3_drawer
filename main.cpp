
#include "Shape.h"
#include <sstream>
#include <cstdlib>
#include <fstream>

#include "drawer.h"

using namespace std;

int main(int argc, char * argv[])
{
    if(argc > 1)
    {
        ifstream f1;
        f1.open(argv[1]);
        string out = argv[2];
        drawer(f1,out);
        f1.close();
    }
    else
    {
        string out = "example.bmp";
        drawer(cin,out);
    }

    return 0;
}
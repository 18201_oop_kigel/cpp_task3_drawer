
#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector> //вместо динамического массива
#include "Characteristic.h"
#include <memory>

using namespace std;

class Segments
{
private:
    pair <int,int> start;
    pair <int,int> end;
public:
    Segments()
    {
        start = make_pair(0,0);
        end = make_pair(0,0);

    }
    Segments(int x1, int y1, int x2, int y2)
    {
        start = make_pair(x1,y1);
        end = make_pair(x2,y2);
    }
    Segments(pair<int, int> x, pair <int, int> y)
    {
        start = x;
        end = y;
    }
    pair<int,int> getStart() const;
    pair<int,int> getEnd() const;
};

class Shape: public Named, public Colored
{
protected:
    pair <int, int> center;
public:
    virtual ~Shape() = default;
    virtual void setCenter(int, int) = 0;
    virtual double square () const = 0;
    virtual vector<Segments> getSegments() const = 0;
    string getName() const;
};
#endif
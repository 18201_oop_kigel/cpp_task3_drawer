//
// Created by 79137 on 11.12.2019.
//

#ifndef CPP_TASK3_DRAWER_DRAWER_H
#define CPP_TASK3_DRAWER_DRAWER_H

#include <iostream>
#include "Shape.h"
#include "Types.h"
#include <string.h>
#include <cstdlib>
#include "bitmap_image.hpp"
#include "dop.h"

using namespace std;

void drawer(std::istream & in, std::string & out);

#endif //CPP_TASK3_DRAWER_DRAWER_H


//
// Created by 79137 on 17.12.2019.
//

#include "dop.h"

std::vector<unsigned int> StColor_in_Int(const string & s)
{
   std::vector <unsigned int > color;
    if(s == "black")
    {
        color.push_back(0);
        color.push_back(0);
        color.push_back(0);
    }
    if(s == "white")
    {
        color.push_back(255);
        color.push_back(255);
        color.push_back(255);
    }

    if(s == "red")
    {
        color.push_back(255);
        color.push_back(0);
        color.push_back(0);
    }
    if(s == "lime")
    {
        color.push_back(0);
        color.push_back(255);
        color.push_back(0);
    }

    if(s == "blue")
    {
        color.push_back(0);
        color.push_back(0);
        color.push_back(255);
    }

    if(s == "yellow")
    {
        color.push_back(255);
        color.push_back(255);
        color.push_back(0);
    }

    if(s == "aqua")
    {
        color.push_back(0);
        color.push_back(255);
        color.push_back(255);
    }

    if(s == "magenta")
    {
        color.push_back(255);
        color.push_back(0);
        color.push_back(255);
    }

    if(s == "silver")
    {
        color.push_back(192);
        color.push_back(192);
        color.push_back(192);
    }

    if(s == "gray")
    {
        color.push_back(128);
        color.push_back(128);
        color.push_back(128);
    }

    if(s == "maroon")
    {
        color.push_back(128);
        color.push_back(0);
        color.push_back(0);
    }

    if(s == "olive")
    {
        color.push_back(128);
        color.push_back(128);
        color.push_back(0);
    }

    if(s == "green")
    {
        color.push_back(0);
        color.push_back(128);
        color.push_back(0);
    }

    if(s == "purple")
    {
        color.push_back(128);
        color.push_back(0);
        color.push_back(128);
    }

    if(s == "teal")
    {
        color.push_back(0);
        color.push_back(128);
        color.push_back(128);
    }

    if(s == "navy")
    {
        color.push_back(0);
        color.push_back(0);
        color.push_back(128);
    }
    return color;
}

//
// Created by 79137 on 02.12.2019.
//

#include "Characteristic.h"


std::string my_to_string(int n)
{
    std::ostringstream ss;
    ss << n;
    return ss.str();
}

std::map <std::string, unsigned int> Named::count;

void Colored::setColor(unsigned int red,unsigned int green,unsigned int blue)
{
    r = red;
    g = green;
    b = blue;
}

void Colored::setColor(const std::string & s)
{
    if(s == "black")
    {
        setColor(0,0,0);
        return;
    }
    if(s == "white")
    {
        setColor(255,255,255);
        return;
    }

    if(s == "red")
    {
        setColor(255,0,0);
        return;
    }
    if(s == "lime")
    {
        setColor(0,255,0);
        return;
    }

    if(s == "blue")
    {
        setColor(0,0,255);
        return;
    }

    if(s == "yellow")
    {
        setColor(255,255,0);
        return;
    }

    if(s == "aqua")
    {
        setColor(0,255,255);
        return;
    }

    if(s == "magenta")
    {
        setColor(255,0,255);
        return;
    }

    if(s == "silver")
    {
        setColor(192,192,192);
        return;
    }

    if(s == "gray")
    {
        setColor(128,128,128);
        return;
    }

    if(s == "maroon")
    {
        setColor(128,0,0);
        return;
    }

    if(s == "olive")
    {
        setColor(128,128,0);
        return;
    }

    if(s == "green")
    {
        setColor(0,128,0);
        return;
    }

    if(s == "purple")
    {
        setColor(128,0,128);
        return;
    }

    if(s == "teal")
    {
        setColor(0,128,128);
        return;
    }

    if(s == "navy")
    {
        setColor(0,0,128);
        return;
    }
}

std::tuple<int,int,int> Colored::getColored()
{
    return std::make_tuple(r,g,b);
}

Named::Named(const std::string &type)
{

    if(count[type] == 0)
    {
        count[type] = 1;
    }

    std::string res = type + "." + my_to_string(count[type]);
    count[type]++;
    name = res;
}